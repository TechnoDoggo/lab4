public class Android
{
	private double AmountOfMemory;
	private String Model;
	private String AndroidVersion;
	
	public Android(double AmountOfMemory, String Model, String AndroidVersion)
	{
		this.AmountOfMemory = AmountOfMemory;
		this.Model = Model;
		this.AndroidVersion = AndroidVersion;
	}
	
	public void MemoryCheck(double Memory)
	{
		double TrueMemory = (Memory - 2.3);
		System.out.println("With the OS of the Android phone taking up around 2.3GBs, the true amount of memory that the phone has is " + TrueMemory + ". We are sorry for the confusion.");
	}
	
	public double getAmountOfMemory()
	{
		return this.AmountOfMemory;
	}
	
	public String getModel()
	{
		return this.Model;
	}
	
	public String getAndroidVersion()
	{
		return this.AndroidVersion;
	}
	
	public void setAmountOfMemory(double newAmountOfMemory)
	{
		this.AmountOfMemory = newAmountOfMemory;
	}
	
	public void setModel(String newModel)
	{
		this.Model = newModel;
	}
	
	public void setAndroidVersion(String newAndroidVersion)
	{
		this.AndroidVersion = newAndroidVersion;
	}
}
