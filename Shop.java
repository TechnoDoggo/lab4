public class Shop
{
	public static void main(String[] args)
	{
		java.util.Scanner reader = new
		java.util.Scanner(System.in);
		
		Android Phones = new Android(0, "test", "test");
		Android TrueMemory = new Android(0, "test", "test");
		
		Android[] products = new Android[4];
		
		for (int i = 0; i < 4; i++)
		{
			System.out.println("Current Product: Android Phone #" + (i + 1));
			
			System.out.println("Please input the memory of the Android Phone in Gigabites (GBs).");
			String MemoryString = reader.nextLine();
			double AmountOfMemory = (Double.parseDouble(MemoryString));
			
			System.out.println("Please input the model of the Android Phone.");
			String Model = (reader.nextLine());
			
			System.out.println("Please input the current version of Android that is currently running on the Android Phone.");
			String AndroidVersion = reader.nextLine();
			
			Phones = new Android(AmountOfMemory, Model, AndroidVersion);
			products[i] = Phones;
		}
		
		System.out.println("Info on 4th Android Phone.");
		System.out.println("The memory of the Android Phone is : " + products[3].getAmountOfMemory() + " GBs");
		System.out.println("The model of the Android Phone is : " + products[3].getModel());
		System.out.println("The current version of the Android Phone is : " + products[3].getAndroidVersion());
		
		System.out.println("Sorry, looks like an error has occured. Please input new information.");
		System.out.println("Please input the new memory of the Android Phone is Gigabites (GBs).");
		String ErrorMemoryString = reader.nextLine();
		Phones.setAmountOfMemory(Double.parseDouble(ErrorMemoryString));
		
		System.out.println("Please input the new model of the Android Phone.");
		Phones.setModel(reader.nextLine());
		
		System.out.println("Please input the new version of Android that is currently running on the Android Phone.");
		Phones.setAndroidVersion(reader.nextLine());
		
		double ErrorAmountOfMemory = Phones.getAmountOfMemory();
		String ErrorModel =  Phones.getModel();
		String ErrorAndroidVersion = Phones.getAndroidVersion();
		
		Phones = new Android(ErrorAmountOfMemory, ErrorModel, ErrorAndroidVersion);
		products[3] = Phones;
		System.out.println("Corrected Info on 4th Android Phone.");
		System.out.println("The memory of the Android Phone is : " + products[3].getAmountOfMemory() + " GBs");
		System.out.println("The model of the Android Phone is : " + products[3].getModel());
		System.out.println("The current version of the Android Phone is : " + products[3].getAndroidVersion());
		
		TrueMemory.MemoryCheck(products[3].getAmountOfMemory());
	}
}